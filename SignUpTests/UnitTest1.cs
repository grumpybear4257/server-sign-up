using System;
using SystemObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SignUpTests {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void TestCreateHost_JustName() {
            Host host = new Host("test", "test@example.com");

            Assert.IsNotNull(host);
            Assert.AreEqual("test", host.Name);
        }

        [TestMethod]
        public void TestCreateHost_NameWithSite() {
            Host host = new Host("test", "http://example.com");

            Assert.IsNotNull(host);
            Assert.AreEqual("test", host.Name);
        }

        [TestMethod]
        public void TestCreateHost_NoName() {
            Host host = null;
            try {
                host = new Host(string.Empty, "test@example.com");
            } catch (ArgumentNullException) {
                // Ignored
            }
            
            Assert.IsNull(host);
        }

        [TestMethod]
        public void TestSetHostPhone() {
            try {
                Host host = new Host("test", "", "", "+(123)-456-7890");
                Assert.AreEqual("1234567890", host.Phone);

                host = new Host("test", "", "", "(456)-123-7890");
                Assert.AreEqual("4561237890", host.Phone);

                host = new Host("test", "", "", "123-456-7890");
                Assert.AreEqual("1234567890", host.Phone);

                host = new Host("test", "", "", "(456)1237890");
                Assert.AreEqual("4561237890", host.Phone);

                host = new Host("test", "", "", "0987654321");
                Assert.AreEqual("0987654321", host.Phone);

                host = new Host("test", "test@example.com");
                Assert.AreEqual(string.Empty, host.Phone);
            } catch (ArgumentException) {
                Assert.Fail();
            }
        }
    }
}
