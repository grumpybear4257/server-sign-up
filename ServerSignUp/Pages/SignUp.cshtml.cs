using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using SystemObjects;

namespace ServerSignUp.Pages {
    public class SignUpModel : PageModel {
        [BindProperty]
        public User TempUser { get; set; }

        public Host Host;
        public string ResultInfo;

        private List<Host> hosts = new List<Host>();

        public void OnGet(string id) {
            hosts = Database.GetHosts();
            int.TryParse(id, out int ID);
            Host = hosts.FirstOrDefault(hst => hst.Id == ID);
            HttpContext.Session.SetInt32("HostID", ID);
        }

        public IActionResult OnPost() {
            int? id = HttpContext.Session.GetInt32("HostID");
            hosts = Database.GetHosts();
            Host = hosts.FirstOrDefault(hst => hst.Id == id);
            List<SignUp> signUps = Database.GetSignUps();
            User newUser;

            if (Host == null) {
                ResultInfo = "An error has occured. Host is null. Please try again.";
                return Page(); //Inform user an error occured.
            }

            if (ModelState.IsValid) {
                List<User> users = Database.GetUsers();
                newUser = users.FirstOrDefault(usr => usr.Email == TempUser.Email);

                if (newUser == null) {
                    Database.AddUser(TempUser);
                    users = Database.GetUsers();
                    newUser = users.FirstOrDefault(usr => usr.Email == TempUser.Email);
                }
            } else {
                ResultInfo = "Not all information has been entered correctly.";
                return Page();
            }

            if (Database.CanServerBeAddedTo(Host)) {
                Database.CreateSignUp(Host, newUser, "");
                return RedirectToPage("Completed");
            }

            ResultInfo = "Server is currently full, please try a different host.";
            return Page();
        }
    }
}