﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SystemObjects;

namespace ServerSignUp.Pages {
    public class IndexModel : PageModel {
        public List<Host> hosts = new List<Host>();

        public void OnGet() {
            hosts = Database.GetHosts();
        }

        public IActionResult OnPostCustom(int Id) {
            return RedirectToPage("SignUp", new { id = Id });
        }
    }
}
