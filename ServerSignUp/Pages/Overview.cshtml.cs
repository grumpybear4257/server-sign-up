using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SystemObjects;

namespace ServerSignUp.Pages {
    public class OverviewModel : PageModel {

        public List<SignUp> signUps = new List<SignUp>();
        public List<Host> hosts = new List<Host>();

        public void OnGet() {
            signUps = Database.GetSignUps();
            hosts = Database.GetHosts();
        }
    }
}