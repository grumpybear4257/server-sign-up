﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SystemObjects;
using SystemObjects.Utils;
using Microsoft.Extensions.Configuration;

namespace ServerSignUp {
    public static class Database {
        public static IConfiguration Configuration;

        private static string connectionString;
        private static int minimumUsersRequired;

        public static void Init() {
            string server = Configuration["AppSettings:db:server"];
            string database = Configuration["AppSettings:db:database"];
            string username = Configuration["AppSettings:db:username"];
            string password = Configuration["AppSettings:db:password"];
            int.TryParse(Configuration["AppSettings:db:MinimumRequiredUsers"], out int MinReq);
            minimumUsersRequired = MinReq;
            
            connectionString = $"server={server};Database={database};user id={username};password={password}";
        }

        public static List<Host> GetHosts() {
            try {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM Groups", connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command)) {
                    List<Host> hosts = new List<Host>();
                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    foreach (DataRow row in data.Rows)
                        hosts.Add(Host.GetHostFromDataRow(row));

                    return hosts;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return new List<Host>();
            }
        }

        public static List<User> GetUsers() {
            try {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM Users", connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command)) {
                    List<User> users = new List<User>();
                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    foreach (DataRow row in data.Rows)
                        users.Add(User.GetUserFromDataRow(row));

                    return users;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return new List<User>();
            }
        }

        public static bool AddUser(User user) {
            try {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("INSERT INTO Users(FirstName, LastName, Email, Phone) VALUES(@FirstName, @LastName, @Email, @Phone)", connection)) {
                    connection.Open();

                    command.Parameters.AddWithValue("@FirstName", user.FirstName);
                    command.Parameters.AddWithValue("@LastName", user.LastName);
                    command.Parameters.AddWithValue("@Email", user.Email);
                    command.Parameters.AddWithValue("@Phone", user.Phone);

                    command.ExecuteNonQuery();

                    return true;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public static bool CreateSignUp(Host host, User user, string joinReason) {
            try {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("INSERT INTO Signups(GroupID, UserID, SignupTime, Reason) VALUES(@GroupID, @UserID, @SignupTime, @Reason)", connection)) {
                    connection.Open();

                    command.Parameters.AddWithValue("@GroupID", host.Id);
                    command.Parameters.AddWithValue("@UserID", user.ID);
                    command.Parameters.AddWithValue("@SignupTime", DateTime.Now);
                    command.Parameters.AddWithValue("@Reason", joinReason);

                    command.ExecuteNonQuery();

                    return true;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public static List<SignUp> GetSignUps() {
            try {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM Signups", connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command)) {
                    List<SignUp> signUps = new List<SignUp>();
                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    foreach (DataRow row in data.Rows)
                        signUps.Add(SignUp.GetSignupFromDataRow(row));

                    return signUps;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return new List<SignUp>();
            }
        }

        public static bool CanServerBeAddedTo(Host host) {
            LimitStillActive();
            if (HostSignUpCount(host) < minimumUsersRequired && LimitStillActive())
                return true;
            if (LimitStillActive() == false)
                return true;

            return false;
        }

        private static int HostSignUpCount(Host host) {
            List<SignUp> signUps = GetSignUps();
            int retVal = 0;
            foreach (SignUp signUp in signUps) {
                if (signUp.Host.Id == host.Id)
                    retVal++;
            }
            return retVal;
        }

        private static bool LimitStillActive() {
            List<Host> hosts = GetHosts();
            bool retVal = false;
            foreach (Host host in hosts) {
                if (retVal == false) {
                    if (HostSignUpCount(host) < minimumUsersRequired)
                        retVal = true;
                }
            }
            return retVal;
        }
    }
}
