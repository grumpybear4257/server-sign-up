﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using SystemObjects;

namespace ServerSignUp {
    public class SignUp {
        public User User { get; set; }
        public Host Host { get; set; }
        public DateTime SignUpTime { get; set; }
        public string SignUpReason { get; set; }

        public SignUp(Host host, User user, DateTime signUpTime, string reason) {
            Host = host;
            User = user;
            SignUpTime = signUpTime;
            SignUpReason = reason;
        }

        public static SignUp GetSignupFromDataRow(DataRow data) {
            List<Host> hosts = Database.GetHosts();
            List<User> users = Database.GetUsers();
            Host host = hosts.Where(hst => hst.Id == int.Parse(data["GroupID"].ToString())).FirstOrDefault();
            User user = users.Where(usr => usr.ID == int.Parse(data["UserID"].ToString())).FirstOrDefault();
            DateTime signupTime = DateTime.Parse(data["SignupTime"].ToString());
            string reason = data["Reason"].ToString();
            
            return new SignUp(host, user, signupTime, reason);
        }
    }
}
