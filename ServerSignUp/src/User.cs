﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ServerSignUp {
    public class User {
        public int ID { get; set; }
        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First Name:")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last Name:")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [Display(Name = "Email:")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(mymhc)(\.com|.ca)$", ErrorMessage = "Please use your mymhc email.") ]
        public string Email { get; set; }
        [Display(Name = "Phone:")]
        [Phone]
        public string Phone { get; set; }

        public User(int id, string firstName, string lastName, string email, string phone) {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Phone = phone;
        }
        public User() {
        }

        public static User GetUserFromDataRow(DataRow data) {
            int id = int.Parse(data["UserID"].ToString());
            string firstName = data["FirstName"].ToString();
            string lastName = data["LastName"].ToString();
            string email = data["Email"].ToString();
            string phone = data["Phone"].ToString();

            return new User(id, firstName, lastName, email, phone);
        }
    }
}
