﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace SystemObjects {
    public class Host {
        public static List<Host> Hosts = new List<Host>();
        private string email, phone, site;

        public Host(string groupName, string email, string groupSite = "", string phone = "") {
            if (string.IsNullOrWhiteSpace(groupName))
                throw new ArgumentNullException(nameof(groupName), "Group Name cannot be empty!");

            if (Hosts.Any(h => h.Name == groupName))
                throw new ArgumentException("Group Name already taken!", nameof(groupName));

            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email), "Email cannot be empty!");

            if (groupSite.Trim().Length > 100)
                throw new ArgumentOutOfRangeException(nameof(groupSite), "Group URL cannot exceed 100 characters!");

            Regex emailRegex= new Regex("^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$");
            if (emailRegex.IsMatch(email.ToLower()))
                Email = email;
            else
                throw new ArgumentException("Invalid Email!", nameof(email));
            
            if (string.IsNullOrWhiteSpace(groupSite) == false) {
                // From https://www.regextester.com/96504
                Regex regex = new Regex(@"(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))?");
                if (regex.IsMatch(groupSite))
                    site = groupSite;
                else
                    throw new ArgumentException("Invalid URL for Group Site!", nameof(groupSite));
            } else
                site = string.Empty;

            if (string.IsNullOrWhiteSpace(phone) == false) {
                // From https://regexr.com/3c53v
                Regex regex = new Regex(@"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$");
                if (regex.IsMatch(phone))
                    // Strip out all of the non-numeric characters (eg +(123) - 456 - 7890 -> 1234567890)
                    this.phone = new string(phone.Where(char.IsDigit).ToArray());
                else
                    throw new ArgumentException("Invalid Phone Number!", nameof(phone));
            } else
                this.phone = string.Empty;
            
            Name = groupName;
            Hosts.Add(this);
        }

        private Host(int id, string name, string email, string site, string phone) {
            Id = id;
            Name = name;
            Email = email;
            this.site = site;
            this.phone = phone;
        }

        public static Host GetHostFromDataRow(DataRow data) {
            int id = int.Parse(data["GroupID"].ToString());
            string name = data["Name"].ToString();
            string email = data["Email"].ToString();
            string site = data["URL"].ToString();
            string phone = data["Phone"].ToString();

            return new Host(id, name, email, site, phone);
        }

        public int Id { get; }

        public string Name { get; }

        public string Email {
            get => email;
            set {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException(nameof(value), "Email cannot be empty!");

                Regex emailRegex = new Regex("^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$");
                if (emailRegex.IsMatch(value.ToLower()))
                    email = value;
                else
                    throw new ArgumentException("Invalid Email!", nameof(value));
            }
        }

        public string Phone {
            get => phone;
            set {
                if (string.IsNullOrWhiteSpace(value) == false) {
                    // From https://regexr.com/3c53v
                    Regex regex = new Regex(@"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$");
                    if (regex.IsMatch(value))
                        // Strip out all of the non-numeric characters (eg +(123) - 456 - 7890 -> 1234567890)
                        // We do this in the UI, but just in case we don't do it somewhere, we're doing it again here. It shouldn't hurt anything.
                        phone = new string(value.Where(char.IsDigit).ToArray());
                    else
                        throw new ArgumentException("Invalid Phone Number!", nameof(value));
                } else
                    phone = string.Empty;
            }
        }

        public string Site {
            get => site;
            set {
                if (value.Trim().Length > 100)
                    throw new ArgumentOutOfRangeException(nameof(value), "Group URL cannot exceed 100 characters!");

                if (string.IsNullOrWhiteSpace(value) == false) {
                    // From https://www.regextester.com/96504
                    Regex regex = new Regex(@"(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))?");
                    if (regex.IsMatch(value))
                        site = value;
                    else
                        throw new ArgumentException("Invalid URL for Group Site!", nameof(value));
                } else
                    site = string.Empty;
            }
        } 
    }
}
