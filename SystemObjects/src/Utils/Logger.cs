﻿using System;
using System.IO;

namespace SystemObjects.Utils {
    public enum LogLevel {
        Info, Error, Warning
    }

    public static class Logger {
        private static readonly string Folder = Path.Combine(Directory.GetCurrentDirectory(), "logs");
        private static readonly string FileName;

        static Logger() {
            // Make sure the folder exists
            if (Directory.Exists(Folder) == false)
                Directory.CreateDirectory(Folder);
            
            // Set the filename of the log file that we're going to use
            FileName = Path.Combine(Folder, $"{DateTime.Now:dd-MMM-yyyy HHmm}.log");
        }

        public static void Log(LogLevel level, string message) {
            string date = DateTime.Now.ToString("T");

            // Write the log message to the file.
            using (StreamWriter writer = new StreamWriter(FileName, true)) {
                switch (level) {
                    case LogLevel.Info:
                        writer.WriteLine($"{date} :: INFO :: {message}");
                        break;
                    case LogLevel.Error:
                        writer.WriteLine($"{date} :: ERROR :: {message}");
                        break;
                    case LogLevel.Warning:
                        writer.WriteLine($"{date} :: WARN :: {message}");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(level), level, null);
                }
            }
        }
    }
}
