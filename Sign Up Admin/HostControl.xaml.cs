﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SystemObjects;

namespace SignUpAdmin {
    /// <summary>
    /// Interaction logic for HostControl.xaml
    /// </summary>
    public partial class HostControl : UserControl {
        private readonly MainWindow mainWindow;
        private readonly Host host;

        public HostControl(Host host, MainWindow mainWindow) {
            InitializeComponent();

            this.mainWindow = mainWindow;
            this.host = host;

            HostNameLbl.Content = host.Name;
            MemberCountLbl.Content = $"Members: {Database.CountSignUpsOnHost(host)}";
        }

        private void RemoveBtn_Click(object sender, MouseButtonEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to remove this host?", "Confirm Removal", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result != MessageBoxResult.Yes)
                return;

            if (Database.RemoveHost(host) == false)
                MessageBox.Show($"Something went wrong while removing the group: {host.Name}. Please check the logs for more details.");
            mainWindow.UpdateHosts();
        }

        private void ManageBtn_OnClick(object sender, RoutedEventArgs e) {
            ManageHostPage managePage = new ManageHostPage(host);
            managePage.ShowDialog();

            mainWindow.UpdateHosts();
        }
    }
}
