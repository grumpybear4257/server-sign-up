﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SystemObjects;
using SystemObjects.Utils;

namespace SignUpAdmin {
    public static class Database {
        private static readonly string ConnectionString;

        static Database() {
            string server = ConfigurationManager.AppSettings.Get("server");
            string database = ConfigurationManager.AppSettings.Get("database");
            string username = ConfigurationManager.AppSettings.Get("username");
            string password = ConfigurationManager.AppSettings.Get("password");

            ConnectionString = $"server={server};database={database};user id={username};password={password}";

            Logger.Log(LogLevel.Info, "Ensuring database tables are setup for use");
            CreateTables();
        }

        public static void CreateTables() {
            using (SqlConnection connection = new SqlConnection(ConnectionString)) {
                // Setting to null so we can do a null check on the disposal later. This really should never be null though, unless we don't have a connection to the server.
                SqlCommand command = null;
                connection.Open();

                try {
                    Logger.Log(LogLevel.Info, "Creating tables");

                    command = new SqlCommand(@"CREATE TABLE [dbo].[Groups](
                    [GroupID][int] IDENTITY(1, 1) NOT NULL,
                    [Name][varchar](50) NOT NULL,
                    [Email][varchar](100) NOT NULL,
                    [Phone][varchar](10) NULL,
                    [URL][varchar](100) NULL, 
                    PRIMARY KEY (GroupID))", connection);

                    command.ExecuteNonQuery();

                    command = new SqlCommand("ALTER TABLE Groups ADD CONSTRAINT uname UNIQUE(Name)", connection);
                    command.ExecuteNonQuery();

                    Logger.Log(LogLevel.Info, "Created table: Groups");

                    command = new SqlCommand(@"CREATE TABLE [dbo].[Staff](
	                [StaffID] [int] IDENTITY(1,1) NOT NULL,
	                [FirstName] [varchar](100) NOT NULL,
	                [LastName] [varchar](100) NOT NULL,
	                [Email] [varchar](100) NOT NULL,
	                [GroupID] [int] NOT NULL,
                    PRIMARY KEY (StaffID))", connection);

                    command.ExecuteNonQuery();
                    Logger.Log(LogLevel.Info, "Created table: Staff");

                    command = new SqlCommand(@"CREATE TABLE [dbo].[Users](
	                [UserID] [int] IDENTITY(1,1) NOT NULL,
	                [FirstName] [varchar](100) NOT NULL,
	                [LastName] [varchar](100) NOT NULL,
	                [Email] [varchar](100) NOT NULL,
	                [Phone] [varchar](10) NULL,
                    PRIMARY KEY (UserID))", connection);

                    command.ExecuteNonQuery();
                    Logger.Log(LogLevel.Info, "Created table: Users");

                    command = new SqlCommand(@"CREATE TABLE [dbo].[Signups](
                    [GroupID] [int] NOT NULL,
                    [UserID] [int] NOT NULL,
                    [SignupTime] [datetime] NOT NULL,
                    [Reason] [varchar](250) NULL
                    )", connection);

                    command.ExecuteNonQuery();
                    Logger.Log(LogLevel.Info, "Created table: Signups");

                    command = new SqlCommand(
                        "ALTER TABLE [dbo].[Signups] WITH CHECK ADD CONSTRAINT [FK_Signups_Groups] FOREIGN KEY([GroupID]) REFERENCES [dbo].[Groups] ([GroupID])",
                        connection);
                    command.ExecuteNonQuery();

                    command = new SqlCommand("ALTER TABLE [dbo].[Signups] CHECK CONSTRAINT [FK_Signups_Groups]",
                        connection);
                    command.ExecuteNonQuery();

                    command = new SqlCommand(
                        "ALTER TABLE [dbo].[Signups] WITH CHECK ADD CONSTRAINT [FK_Signups_Users] FOREIGN KEY([UserID]) REFERENCES [dbo].[Users] ([UserID])",
                        connection);
                    command.ExecuteNonQuery();

                    command = new SqlCommand("ALTER TABLE [dbo].[Signups] CHECK CONSTRAINT [FK_Signups_Users]",
                        connection);
                    command.ExecuteNonQuery();

                    command = new SqlCommand(
                        "ALTER TABLE [dbo].[Staff] WITH CHECK ADD  CONSTRAINT [FK_Staff_Groups] FOREIGN KEY([GroupID]) REFERENCES [dbo].[Groups] ([GroupID])",
                        connection);
                    command.ExecuteNonQuery();

                    command = new SqlCommand("ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_Groups]",
                        connection);
                    command.ExecuteNonQuery();

                    Logger.Log(LogLevel.Info, "Setup table references and constraints");
                } catch (SqlException sqlEx) {
                    if (sqlEx.Number == 2714)
                        Logger.Log(LogLevel.Info, "Tables already exist");
                    else
                        Logger.Log(LogLevel.Error, sqlEx.Message);
                } catch (Exception ex) {
                    Logger.Log(LogLevel.Error, ex.Message);
                } finally {
                    connection.Close();
                    connection.Dispose();
                    command?.Dispose();
                    Logger.Log(LogLevel.Info, "Finished database table setup");
                }
            }
        }

        public static void DropTables() {
            Logger.Log(LogLevel.Warning, "Dropping all tables!");

            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString)) {
                    connection.Open();

                    SqlCommand cmd = new SqlCommand("DROP TABLE [dbo].[Staff]", connection);
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("DROP TABLE [dbo].[Signups]", connection);
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("DROP TABLE [dbo].[Users]", connection);
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("DROP TABLE [dbo].[Groups]", connection);
                    cmd.ExecuteNonQuery();
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
            }
        }

        public static List<Host> GetHosts() {
            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM Groups", connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command)) {
                    List<Host> hosts = new List<Host>();
                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    foreach (DataRow row in data.Rows)
                        hosts.Add(Host.GetHostFromDataRow(row));

                    return hosts;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return new List<Host>();
            }
        }

        public static bool AddHost(Host host) {
            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("INSERT INTO Groups(Name, Email, Phone, URL) VALUES(@Name, @Email, @Phone, @URL)", connection)) {
                    connection.Open();

                    command.Parameters.AddWithValue("@Name", host.Name);
                    command.Parameters.AddWithValue("@Email", host.Email);
                    command.Parameters.AddWithValue("@Phone", host.Phone);
                    command.Parameters.AddWithValue("@URL", host.Site);

                    command.ExecuteNonQuery();

                    return true;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public static bool RemoveHost(Host host) {
            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("DELETE FROM Groups WHERE Name = @Name", connection)) {
                    connection.Open();
                    command.Parameters.AddWithValue("@Name", host.Name);

                    command.ExecuteNonQuery();

                    return true;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public static bool UpdateHost(Host host) {
            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("UPDATE Groups SET Email = @Email, Phone = @Phone, URL = @URL WHERE Name = @Name", connection)) {
                    connection.Open();

                    command.Parameters.AddWithValue("@Email", host.Email);
                    command.Parameters.AddWithValue("@Phone", host.Phone);
                    command.Parameters.AddWithValue("@URL", host.Site);
                    command.Parameters.AddWithValue("@Name", host.Name);

                    command.ExecuteNonQuery();

                    return true;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public static int CountSignUpsOnHost(Host host) {
            try {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM Signups WHERE GroupID = @ID", connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command)) {
                    command.Parameters.AddWithValue("@ID", host.Id);

                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    return data.Rows.Count;
                }
            } catch (Exception ex) {
                Logger.Log(LogLevel.Error, ex.Message);
                return -1;
            }
        }
    }
}
