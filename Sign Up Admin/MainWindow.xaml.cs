﻿using System.Windows;
using SystemObjects;

namespace SignUpAdmin {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();

            UpdateHosts();
        }

        public void UpdateHosts() {
            Hosts.Children.Clear();
            Host.Hosts = Database.GetHosts();

            foreach (Host host in Host.Hosts)
                Hosts.Children.Add(new HostControl(host, this));
        }

        private void RefreshDatabaseBtn_OnClick(object sender, RoutedEventArgs e) {
            MessageBoxResult confirm = MessageBox.Show(
                "Are you sure you want to reset the database?", "Are you sure?",
                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            if (confirm != MessageBoxResult.Yes)
                return;

            Database.DropTables();
            Database.CreateTables();

            UpdateHosts();
        }

        private void CreateHostBtn_OnClick(object sender, RoutedEventArgs e) {
            CreateHostPage createHost = new CreateHostPage();
            createHost.ShowDialog();

            UpdateHosts();
        }
    }
}
