﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using SystemObjects;

namespace SignUpAdmin {
    /// <summary>
    /// Interaction logic for CreateHostPage.xaml
    /// </summary>
    public partial class CreateHostPage : Window {
        public CreateHostPage() {
            InitializeComponent();
            ResetPage();
        }

        private void CreateGroupBtn_Click(object sender, RoutedEventArgs e) {
            string name = GroupNameTxt.Text.Trim();

            if (string.IsNullOrWhiteSpace(name)) {
                NameErrorLbl.Content = "Required";
                return;
            }

            if (Host.Hosts.Any(h => h.Name == name)) {
                NameErrorLbl.Content = "Already taken";
                return;
            }

            NameErrorLbl.Content = string.Empty;

            if (GetValidGroupEmail(out string email) == false) {
                EmailErrorLbl.Content = "Please enter a valid Email";
                return;
            }

            EmailErrorLbl.Content = string.Empty;

            if (GetValidGroupUrl(out string site) == false) {
                UrlErrorLbl.Content = "Please enter a valid URL";
                return;
            }

            UrlErrorLbl.Content = string.Empty;

            if (GetValidGroupPhone(out string phone) == false) {
                PhoneErrorLbl.Content = "Please enter a valid Phone Number";
                return;
            }

            PhoneErrorLbl.Content = string.Empty;

            if (Database.AddHost(new Host(name, email, site, phone)) == false) {
                MessageBox.Show($"Something went wrong while adding the group: {name}. Please check the logs for more details.");
                return;
            }

            MessageBox.Show($"{name} has been added to the database.");
            ResetPage();
        }

        private void ResetPage() {
            GroupNameTxt.Text = string.Empty;
            GroupEmailTxt.Text = string.Empty;
            GroupUrlTxt.Text = string.Empty;
            GroupPhoneTxt.Text = string.Empty;
            NameErrorLbl.Content = string.Empty;
            EmailErrorLbl.Content = string.Empty;
            UrlErrorLbl.Content = string.Empty;
            PhoneErrorLbl.Content = string.Empty;
        }

        private bool GetValidGroupEmail(out string email) {
            string groupEmail = GroupEmailTxt.Text.Trim();

            if (string.IsNullOrWhiteSpace(groupEmail)) {
                email = string.Empty;
                return false;
            }

            Regex regex = new Regex("^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$");
            if (regex.IsMatch(groupEmail.ToLower()) && groupEmail.Length <= 100) {
                email = groupEmail;
                return true;
            }

            email = string.Empty;
            return false;
        }

        private bool GetValidGroupUrl(out string url) {
            string groupUrl = GroupUrlTxt.Text.Trim();

            if (string.IsNullOrWhiteSpace(groupUrl)) {
                url = string.Empty;
                return true;
            }

            Regex regex = new Regex(@"(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))?");
            if (regex.IsMatch(groupUrl) && groupUrl.Length <= 100) {
                url = groupUrl;
                return true;
            }

            url = string.Empty;
            return false;
        }

        private bool GetValidGroupPhone(out string phone) {
            string groupPhone = GroupPhoneTxt.Text.Trim();

            // Check to see if the input is empty (if they entered nothing, this will pass the check, since you don't need a phone number)
            if (string.IsNullOrWhiteSpace(groupPhone)) {
                phone = string.Empty;
                return true;
            }

            // Only get the digits of the phone number.
            groupPhone = new string(groupPhone.Where(char.IsDigit).ToArray());
            // Check if there is anything in the phone number now (if they entered just letters, this will fail the check, because they entered SOMETHING, but that something wasn't right)
            if (string.IsNullOrWhiteSpace(groupPhone)) {
                phone = string.Empty;
                return false;
            }

            // Check to see if the input is a valid phone number
            Regex regex = new Regex(@"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$");
            if (regex.IsMatch(groupPhone) && groupPhone.Length <= 10) {
                phone = groupPhone;
                return true;
            }

            phone = string.Empty;
            return false;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
