﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using SystemObjects;

namespace SignUpAdmin {
    /// <summary>
    /// Interaction logic for ManageHostPage.xaml
    /// </summary>
    public partial class ManageHostPage : Window {
        private readonly Host host;
        private bool emailUnsaved, siteUnsaved, phoneUnsaved;

        public ManageHostPage(Host host) {
            InitializeComponent();

            this.host = host;

            GroupNameTxt.Text = host.Name;
            GroupEmailTxt.Text = host.Email;
            GroupUrlTxt.Text = host.Site;
            GroupPhoneTxt.Text = host.Phone;

            EmailErrorLbl.Content = string.Empty;
            UrlErrorLbl.Content = string.Empty;
            PhoneErrorLbl.Content = string.Empty;
        }

        private bool UnsavedChanges => emailUnsaved || siteUnsaved || phoneUnsaved;

        private bool SaveChanges(out string error) {
            error = string.Empty;
            string email = GroupEmailTxt.Text.Trim();
            string url = GroupUrlTxt.Text.Trim();
            string phone = GroupPhoneTxt.Text.Trim();

            // Check email field
            if (string.IsNullOrWhiteSpace(email)) {
                EmailErrorLbl.Content = "Please enter a valid Email";
                return false;
            }

            Regex emailRegex = new Regex("^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$");
            if (emailRegex.IsMatch(email.ToLower()) == false) {
                EmailErrorLbl.Content = "Please enter a valid Email";
                return false;
            }

            // Check phone field
            if (string.IsNullOrWhiteSpace(phone) == false) {
                // From https://regexr.com/3c53v
                Regex regex = new Regex(@"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$");
                if (regex.IsMatch(phone))
                    // Strip out all of the non-numeric characters (eg +(123) - 456 - 7890 -> 1234567890)
                    phone = new string(phone.Where(char.IsDigit).ToArray());
                else {
                    PhoneErrorLbl.Content = "Please enter a valid phone number!";
                    return false;
                }
            } else
                phone = string.Empty;

            // Check site field
            if (url.Trim().Length > 100) {
                UrlErrorLbl.Content = "URL too long! (max 100 characters)";
                return false;
            }

            if (string.IsNullOrWhiteSpace(url) == false) {
                // From https://www.regextester.com/96504
                Regex regex = new Regex(@"(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))?");
                if (regex.IsMatch(url) == false) {
                    UrlErrorLbl.Content = "Please enter a valid URL!";
                    return false;
                }
            } else
                url = string.Empty;

            host.Email = email;
            host.Site = url;
            host.Phone = phone;

            if (Database.UpdateHost(host) == false) {
                error = "db";
                return false;
            }

            emailUnsaved = siteUnsaved = phoneUnsaved = false;
            GroupEmailTxt.FontStyle = GroupUrlTxt.FontStyle = GroupPhoneTxt.FontStyle = FontStyles.Normal;

            return true;
        }

        private void SaveBtn_OnClick(object sender, RoutedEventArgs e) {
            if (UnsavedChanges == false)
                return;

            if (SaveChanges(out string error) == false && error == "db") {
                MessageBox.Show("There was an error saving the changes, please view the log for more information.");
                return;
            }

            SavedLbl.Visibility = Visibility.Visible;
            DispatcherTimer timer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(5)};
            timer.Start();
            timer.Tick += (s, args) => {
                timer.Stop();
                SavedLbl.Visibility = Visibility.Hidden;
            };
        }

        private void CancelBtn_OnClick(object sender, RoutedEventArgs e) {
            Close();
        }

        private void GroupEmailTxt_OnKeyUp(object sender, KeyEventArgs e) {
            emailUnsaved = host.Email != GroupEmailTxt.Text.Trim();
            GroupEmailTxt.FontStyle = emailUnsaved ? FontStyles.Italic : FontStyles.Normal;
        }

        private void GroupUrlTxt_OnKeyUp(object sender, KeyEventArgs e) {
            siteUnsaved = host.Site != GroupUrlTxt.Text.Trim();
            GroupUrlTxt.FontStyle = siteUnsaved ? FontStyles.Italic : FontStyles.Normal;
        }

        private void GroupPhoneTxt_OnKeyUp(object sender, KeyEventArgs e) {
            phoneUnsaved = host.Phone != GroupPhoneTxt.Text.Trim();
            GroupPhoneTxt.FontStyle = phoneUnsaved ? FontStyles.Italic : FontStyles.Normal;
        }

        private void ManageHostPage_OnClosing(object sender, CancelEventArgs e) {
            if (UnsavedChanges == false)
                return;

            MessageBoxResult result =
                MessageBox.Show(
                    "You currently have unsaved changes. Do you want to save those changes before exiting?",
                    "Save changes", MessageBoxButton.YesNoCancel);

            switch (result) {
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    break;  
                case MessageBoxResult.No:
                    return;
                case MessageBoxResult.Yes:
                    if (SaveChanges(out string error) == false && error == "db")
                        MessageBox.Show("There was an error saving the changes, please view the log for more information.");

                    return;
                default:
                    // They can't get here, but handle it anyways.
                    MessageBox.Show("Invalid option");
                    e.Cancel = true;
                    break;
            }
        }
    }
}
